from orden_topologico import orden_topologico_bfs
from dijkstra import dijkstra
from viajante_greedy import viajante_greedy
from viajante_backtracking import viajante_backtracking
from prim import mst_prim

########################################################################
#                   FUNCIONES DE LA BIBLIOTECA
########################################################################


def camino_minimo(grafo, origen, destino):
    """Calcula el camino mínimo desde el vértice origen al vértice
    destino.

    :param grafo: Grafo dirigido.
    :param origen: Vértice origen.
    :param destino: Vértice destino.
    :return (list): Devuelve una lista con los vértices que definen el
    camino mínimo desde origen hasta destino.

    Ejemplo:
        >> camino_minimo(grafoSedesMundialRusia, 'Moscu', 'Saransk')
        ['Moscú', 'Samara', 'Saransk']

    """
    camino, peso_total = dijkstra(grafo, origen, destino)
    return camino


def viajante(grafo, origen):
    """ Calcula el recorrido a hacer para resolver de manera óptima el
    problema del viajante.
    Ver: https://en.wikipedia.org/wiki/Travelling_salesman_problem.

    :param grafo:
    :param origen: Vértice origen.
    :return (list): Devuelve una lista con el recorrido.

    Ejemplo:
        >> viajante(grafoSedesMundialRusia, Sochi)
        [Sochi, Volgogrado, Kazan, Nizhni Novgorod, Ekaterinburgo,
         Kaliningrado, San Petesburgo, Saransk, Samara, Moscu,
         Rostov del Don, Sochi]

    """

    recorrido, peso_total = viajante_backtracking(grafo, origen)
    return recorrido


def viajante_aproximado(grafo, origen):
    """ Calcula el recorrido a hacer para resolver de manera aproximada
    el problema del viajante.
    Ver: https://en.wikipedia.org/wiki/Travelling_salesman_problem.

    :param grafo:
    :param origen: Vértice origen.
    :return (list): Devuelve una lista con el recorrido.

    Ejemplo:
        >> viajante_aproximado(grafoSedesMundialRusia, Sochi)
        [Sochi, Volgogrado, Kazan, Nizhni Novgorod, Rostov del Don,
        Samara, Moscu, Kaliningrado, San Petesburgo, Saransk,
        Ekaterinburgo, Sochi]

    """

    recorrido, peso_total = viajante_greedy(grafo, origen)
    return recorrido


def orden_topologico(grafo):
    """Calcula un orden topológico del grafo pasado por parámetro.

    :param grafo: Grafo dirigido.
    :return (list): Devuelve una lista con el orden topológico si
    existe, y None si no existe.

    Ejemplo:
        >> orden_topologico(grafoSedesMundialRusiaConRecomendaciones)
        [Saransk, Ekaterinburgo, Moscu, Kaliningrado, San Petesburgo,
        Kazan, Nizhni Novgorod, Samara, Volgogrado, Sochi
        Rostov del Don]

    """

    return orden_topologico_bfs(grafo)


def arbol_tendido_minimo(grafo):
    """Calcula un árbol de tendido mínimo del grafo pasado por
    parámetro.

    :param grafo: Grafo no dirigido y conexo.
    :return (GrafoNoDirigido): Devuelve un nuevo grafo, que representa
    un árbol de tendido mínimo del grafo original.

    """

    arbol, peso_total = mst_prim(grafo)
    return arbol
