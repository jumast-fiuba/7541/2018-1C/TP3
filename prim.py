import queue
from grafo import GrafoNoDrigido


def mst_prim(grafo):
    """Calcula un árbol de tendido mínimo del grafo pasado por
    parámetro, utilizando el algoritmo de Prim.

    :param grafo: Grafo no dirigido y conexo.
    :return (tuple): Devuelve una tupla. El primer elemento es un nuevo
    grafo que representa un árbol de tendido mínimo del grafo original,
    y el segundo elemento es el peso total del árbol (es decir,
    la suma del peso de todas las aristas del grafo que lo representa).

    """
    inicio = grafo.vertice_aleatorio()

    visitados = set()
    visitados.add(inicio)

    peso_total = 0

    q = queue.PriorityQueue()

    for w in grafo.ver_adyacentes(inicio):
        arista = (inicio, w)
        peso_arista = grafo.obtener_peso_arista(inicio, w)
        q.put((peso_arista, arista))

    arbol = GrafoNoDrigido()
    for vertice in grafo.obtener_vertices():
        arbol.agregar_vertice(vertice, grafo.obtener_info_vertice(vertice))

    while not q.empty():
        peso, arista = q.get()
        v, w = arista
        if w in visitados:
            continue

        peso_arista = grafo.obtener_peso_arista(v, w)
        arbol.agregar_arista(v, w, peso_arista)
        visitados.add(w)
        peso_total += peso_arista

        for u in grafo.ver_adyacentes(w):
            arista = (w, u)
            peso_arista = grafo.obtener_peso_arista(w, u)
            q.put((peso_arista, arista))

    return arbol, peso_total
