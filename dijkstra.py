import math
import queue


def dijkstra(grafo, origen, destino):
    """Calcula el camino mínimo desde el vértice @origen al vértice
    @destino, utilizando el algoritmo de Dijkstra.

    :param grafo: Grafo dirigido.
    :param origen: Vértice origen.
    :param destino: Vértice destino.
    :return (tuple): Devuelve una tupla. El primer elemento es una lista
    con los vértices que definen el camino mínimo, y el segundo elemento
    es el costo total del camino.

    Ejemplo:
        >> camino_minimo(grafoSedesMundialRusia, 'Moscu', 'Saransk')
        (['Moscú', 'Samara', 'Saransk'], 8)

    """
    distancia = {}
    padres = {}

    for vertice in grafo.obtener_vertices():
        distancia[vertice] = math.inf

    distancia[origen] = 0
    padres[origen] = None

    q = queue.PriorityQueue()
    q.put((distancia[origen], origen))

    while not q.empty():
        dist, vertice = q.get()

        for w in grafo.ver_adyacentes(vertice):
            if distancia[vertice] + grafo.obtener_peso_arista(vertice, w) < distancia[w]:
                padres[w] = vertice
                distancia[w] = distancia[vertice] + grafo.obtener_peso_arista(vertice, w)
                q.put((distancia[w], w))

    return list(reversed(_reconstruir_ciclo(padres, origen, destino))), distancia[destino]


def _reconstruir_ciclo(padre, inicio, fin):
    """Reconstruye el camino entre @inicio y @fin a partir del
    diccionario de padres construído por el algoritmo de Dijkstra.

    :param padre: Diccionario de padres.
    :param inicio: Vértice inicial del camino.
    :param fin: Vértice final del camino.
    :return (list): Devuelve una lista con los vértices que definen el
    camino, desde @inicio hasta @fin.
    """
    v = fin
    camino = []
    while v != inicio:
        camino.append(v)
        v = padre[v]
    camino.append(inicio)
    return camino
