import unittest
from grafo import *
from biblioteca import *
from utils import leer_archivo_ciudades
from viajante_backtracking import viajante_backtracking
import time


class TestDijkstra(unittest.TestCase):

    def test_dijjstra(self):

        # arrange
        grafo = GrafoDirigido()
        grafo.agregar_vertice("s")
        grafo.agregar_vertice("t")
        grafo.agregar_vertice("x")
        grafo.agregar_vertice("y")
        grafo.agregar_vertice("z")

        grafo.agregar_arista("s", "t", 10)
        grafo.agregar_arista("s", "y", 5)
        grafo.agregar_arista("t", "x", 1)
        grafo.agregar_arista("t", "y", 2)
        grafo.agregar_arista("x", "z", 4)
        grafo.agregar_arista("y", "t", 3)
        grafo.agregar_arista("y", "z", 2)
        grafo.agregar_arista("y", "x", 9)
        grafo.agregar_arista("z", "s", 7)
        grafo.agregar_arista("z", "x", 6)

        # act
        camino_minimo_s_t = dijkstra(grafo, "s", "t")
        camino_minimo_s_y = dijkstra(grafo, "s", "y")
        camino_minimo_s_x = dijkstra(grafo, "s", "x")
        camino_minimo_s_z = dijkstra(grafo, "s", "z")

        # assert
        self.assertEqual(camino_minimo_s_t, (["s", "y", "t"], 8))
        self.assertEqual(camino_minimo_s_y, (["s", "y"], 5))
        self.assertEqual(camino_minimo_s_x, (["s", "y", "t", "x"], 9))
        self.assertEqual(camino_minimo_s_z, (["s", "y", "z"], 7))


class TetstPrim(unittest.TestCase):

        def test_prim(self):

            # arrange
            grafo = GrafoNoDrigido()
            grafo.agregar_vertice("a")
            grafo.agregar_vertice("b")
            grafo.agregar_vertice("c")
            grafo.agregar_vertice("d")
            grafo.agregar_vertice("e")
            grafo.agregar_vertice("f")
            grafo.agregar_vertice("g")
            grafo.agregar_vertice("h")
            grafo.agregar_vertice("i")

            grafo.agregar_arista("a", "b", 4)
            grafo.agregar_arista("a", "h", 8)
            grafo.agregar_arista("b", "c", 8)
            grafo.agregar_arista("b", "h", 11)
            grafo.agregar_arista("c", "d", 7)
            grafo.agregar_arista("c", "f", 4)
            grafo.agregar_arista("c", "i", 2)
            grafo.agregar_arista("d", "e", 9)
            grafo.agregar_arista("d", "f", 14)
            grafo.agregar_arista("e", "f", 10)
            grafo.agregar_arista("f", "g", 2)
            grafo.agregar_arista("g", "h", 1)
            grafo.agregar_arista("g", "i", 6)
            grafo.agregar_arista("h", "i", 7)

            # act
            arbol, peso_total = mst_prim(grafo)

            # assert
            self.assertEqual(peso_total, 37)


class TestViajanteBackTracking(unittest.TestCase):

    def test_viajante(self):

        # arrange
        grafo = leer_archivo_ciudades("sedes.csv")
        peso_optimo = 43

        # act
        VERTICE_ORIGEN = "Saransk"
        inicio = time.time()
        camino, peso = viajante_backtracking(grafo, VERTICE_ORIGEN)
        fin = time.time()
        tiempo_transcurrido_en_segundos = fin - inicio

        # assert
        self.assertEqual(peso, peso_optimo)
        self.assertEqual(len(camino), grafo.cantidad_vertices() + 1)
        self.assertEqual(camino[0], VERTICE_ORIGEN)
        self.assertEqual(camino[0], camino[-1])
        self.assertTrue(tiempo_transcurrido_en_segundos <= 10)


if __name__ == "__main__":
    unittest.main()
