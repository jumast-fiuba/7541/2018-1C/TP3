import math
from grafo import *
from viajante_greedy import viajante_greedy


def viajante_backtracking(grafo, origen):
    """ Calcula el recorrido a hacer para resolver de manera óptima el
    problema del viajante, utilizando la técnica backtracking.
    Ver: https://en.wikipedia.org/wiki/Travelling_salesman_problem.

    :param grafo:
    :param origen: Vértice origen.
    :return (tuple): Devuelve una tupla. El primer elemento es una lista
    de todos los vértices que definen el recorrido, y el segundo
    elemento es el costo total del recorrido.

    Ejemplo:
    >> viajante_backtracking(grafoSedesMundialRusia, Sochi)
    ([Sochi, Volgogrado, Kazan, Nizhni Novgorod, Ekaterinburgo,
     Kaliningrado, San Petesburgo, Saransk, Samara, Moscu,
     Rostov del Don, Sochi], 43)

    """

    solucion, costo_optimo = viajante_greedy(grafo, origen)
    while True:
        solucion_parcial = [origen]
        if _viajante_r(grafo, origen, solucion_parcial, costo_optimo, 0):
            solucion = solucion_parcial
            costo_optimo = calcular_costo_total(grafo, solucion)
        else:
            return solucion, costo_optimo


def _viajante_r(grafo, origen, solucion, costo_optimo, costo_actual):

    vertice = solucion[-1]

    if len(solucion) == grafo.cantidad_vertices():

        peso_arista = grafo.obtener_peso_arista(vertice, origen)
        costo_futuro = costo_actual + peso_arista

        if costo_futuro < costo_optimo:
            solucion.append(origen)
            return True
        else:
            return False

    # puedo agregar vértice?
    for adyacente in grafo.ver_adyacentes(vertice):

        if adyacente in solucion:
            continue

        peso_arista = grafo.obtener_peso_arista(vertice, adyacente)
        costo_futuro = costo_actual + peso_arista

        if costo_futuro < costo_optimo:

            solucion.append(adyacente)
            if _viajante_r(grafo, origen, solucion, costo_optimo, costo_futuro):
                return True

            else:
                # backtrack
                solucion.pop()

    return False
