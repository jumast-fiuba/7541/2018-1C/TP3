import random
from abc import abstractmethod


########################################################################
#                             GRAFO
########################################################################

class GrafoBase:

    """ Clase base para grafos dirigidos y no dirigidos.

    Permite:
        * Agregar un vértice.
        * Borrar un vértice.
        * Verificar si un vértice pertenece al grafo.
        * Obtener todos los vértices.
        * Obtener un vértice aleatorio.
        * Vertificar si un vértice es adyacente a otro.
        * Obtener todos los vértices adyacentes a un vértice dado.
        * Obtener la cantidad total de vértices.
        * Agregar una arista con su peso asociado.
        * Borrar una arista.
        * Borrar todas las aristas.
        * Obtener el peso de una arista.
        * Guardar y obtener información asociada a un vértice.

    """

    def __init__(self):
        self._vertices = {}
        """dict: Diccionario cuyas claves son los vértices del grafo, y 
        cuyos valores son a su vez diccionarios que tienen como claves 
        los vértices adyacentes y como valores el peso de la arista
        correspondiente.
        
        Ejemplo:
        
        Para un grafo cuyos vértices son las sedes del mundial de Rusia.
        
        self._vertices = {
        "Moscu": {"San Petesburgo": 4, "Kaliningrado": 3, "Sochi": 34},
        "San Petesburgo": {"Moscu": 4, "Kaliningrado": 4, "Sochi": 31},
        "Kazan": {"Moscu": 13, "Volgogrado": 2, "Samara": 7}
        }
        
        Los adyacentes de Moscu son San Petesburgo, Kaliningrado y 
        Sochi. El peso de la arista Moscu -> Sochi es 34.
        
        Los adyacentes de Kazan son Moscu, Volgogrado y Samara. El peso
        de la arista Kazan -> Volgogrado es 2.
        """

        self._cantidad_vertices = 0
        """int: Cantidad de vértices del grafo.
        
        """

        self._info_adicional = {}
        """dict: Diccionario para almacenar información asociada a los
        vértices. Cada clave es un vértice del grafo, y cada valor es
        información asociada al vértice correspondiente. La información
        puede ser de cualquier tipo, es responsabilidad del usuario
        saber qué guarda y cómo manipularla.
        
        Ejemplo:
        
        Para un grafo cuyos vértices son las sedes del mundial de Rusia.
        
        self._info_adicional = {
            "Moscu": 5,
            "San Petesburgo": 7,
            "Kazan": 6
        }
        
        En este caso la información adicional es la cantidad de partidos
        jugados en cada ciudad. En Kazan se juegan seis partidos en 
        total.
        
        """

    def agregar_vertice(self, vertice_nombre, vertice_info=None):
        """Agrega al grafo el vértice pasado por parámetro.

        :param vertice_nombre: Vértice a agregar.
        :param vertice_info: Informacion adicional del vértice.
        :return: None
        :raises ValueError: Si el vértice ya pertenece al grafo.
        """

        if self.vertice_en_grafo(vertice_nombre):
            msg = "El vértices {} ya pertenece al grafo".format(vertice_nombre)
            raise ValueError(msg)

        self._vertices[vertice_nombre] = {}
        self._info_adicional[vertice_nombre] = vertice_info
        self._cantidad_vertices += 1

    def es_adyacente(self, w, v):
        """ Verifica si el vértice w es adyacente al vértice v.

        :param v: Vértice.
        :param w: Vértice.
        :return: True si w es adyacente a v, False en caso contrario.
        :raises: ValueError si alguno de los vertices no pertenece al
        grafo.
        """

        if not self.vertice_en_grafo(v):
            msg = "No se puede verificar la adyacencia. El vertice {} no pertence al grafo".format(v)
            raise ValueError(msg)

        if not self.vertice_en_grafo(w):
            msg = "No se puede verificar la adyacencia. El vertice {} no pertence al grafo".format(w)
            raise ValueError(msg)

        return w in self.ver_adyacentes(v)

    def obtener_info_vertice(self, v):
        """Obtiene la información asociada al vértice pasado por
        parámetro.

        :param v: Vértice cuya información se desea obtener.
        :return (object): Informacion adicional asociada al vértice.
        :raises: ValueError si el vértice no pertenece al grafo.
        """

        if not self.vertice_en_grafo(v):
            msg = "El vértice {} no pertenece al grafo".format(v)
            raise ValueError(msg)

        return self._info_adicional[v]

    @abstractmethod
    def borrar_vertice(self, v):
        """Borra del grafo el vértice pasado por parámetro.

        :param v: Vértice que se quiere borrar.
        :return None:
        :raises ValueError si el vértice no pertenece al grafo.

        """

        if not self.vertice_en_grafo(v):
            msg = "El vértice {} no pertence al grafo".format(v)
            raise ValueError(msg)

    @abstractmethod
    def agregar_arista(self, v, w, peso=1):
        """Agrega al grafo la arista pasada por parámetro.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :param peso: Peso asociado a la arista.
        :return: None.
        :raises: ValueError si alguno de los vértices no pertenece al
        grafo.

        """

        if not self.vertice_en_grafo(v):
            msg = "No se puede agregar la arista {} -> {}. El vertice {} no pertence al grafo".format(v, w, v)
            raise ValueError(msg)

        if not self.vertice_en_grafo(w):
            msg = "No se puede agregar la arista {} -> {}. El vertice {} no pertence al grafo".format(v, w, w)
            raise ValueError(msg)

    @abstractmethod
    def borrar_arista(self, v, w):
        """Borra del grafo la arista pasada por parámetro.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :return: None
        :raises:
            ValueError si alguno de los vértices no pertence al grafo.
            ValueError si la arista no pertenece al grafo.

        """

        if not self.vertice_en_grafo(v):
            msg = "No se puede borrar la arista {} - {}. El vertice {} no pertence al grafo".format(v, w, v)
            raise ValueError(msg)

        if not self.vertice_en_grafo(w):
            msg = "No se puede borrar la arista {} -> {}. El vertice {} no pertence al grafo".format(v, w, w)
            raise ValueError(msg)

        if not self.es_adyacente(w, v):
            msg = "No se puede borrar la arista {}-{}, no pertence al grafo".format(v, w)
            raise ValueError(msg)

    def borrar_aristas(self):
        """Borra todas las aristas del grafo.

        :return: None.
        :raises: ValueError si el grafo no tiene vértices.
        """

        if self._vertices == {}:
            raise ValueError()

        for v in self._vertices:
            self._vertices[v] = {}

    def vertice_en_grafo(self, v):
        """Verifica si el vértice pasado por parámetro pertenece al
        grafo.

        :param v: Vértice que se quiere verificar.
        :return: True si el vértice pertenece al grafo, False si no.

        """
        return v in self._vertices

    def obtener_peso_arista(self, v, w):
        """Devuelve el peso de la arista pasada por parámetro.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :return: Peso de la arista, si la arista pertence al grafo.
        :raises:
            ValueError si alguno de los vértices no pertence al grafo.
            ValueError si la arista no pertence al grafo.
        """

        if not self.vertice_en_grafo(v):
            msg = "No se puede obtener el peso. El vertice {} no pertenece al grafo".format(v)
            raise ValueError(msg)

        if not self.vertice_en_grafo(w):
            msg = "No se puede obtener el peso. El vertice {} no pertenece al grafo".format(w)
            raise ValueError(msg)

        if not self.es_adyacente(w, v):
            msg = "No se puede obtener el peso de la arista. {} no es adyacente a {}".format(w, v)
            raise ValueError(msg)

        return self._vertices[v][w]

    def obtener_vertices(self):
        """Devuelve un conjunto (set) con todos los vértices del grafo.

        """

        if self._vertices == {}:
            return set()

        return set(self._vertices.keys())

    def vertice_aleatorio(self):
        """Devuelve un vértice perteneciente al grafo tomado
        aleatoriamente.

        :raises: ValueError si el grafo no tiene vértices.

        """

        if self._vertices == {}:
            raise ValueError()

        return random.choice(list(self._vertices.keys()))

    def ver_adyacentes(self, v):
        """Devuelve un conjunto con todos los vértices adyacentes
        al vértice pasado por parámetro.

        :param v: Vértice cuyos adyacentes se quieren obtener.
        :return (set):
        :raises: ValueError si el vértice pasado por parámetro no
        pertence al grafo.

        """

        if not self.vertice_en_grafo(v):
            msg = "El vertice {} no pertence al grafo".format(v)
            raise ValueError(msg)

        return set(self._vertices[v].keys())

    def cantidad_vertices(self):
        """Devuelve la cantidad total de vértices del grafo.

        """

        return self._cantidad_vertices

    @abstractmethod
    def obtener_peso_total(self):
        """Devuelve la suma del peso de todas las aristas del grafo. """

        suma_pesos = 0

        for vertice in self.obtener_vertices():
            for adyacente in self.ver_adyacentes(vertice):
                suma_pesos += self.obtener_peso_arista(vertice, adyacente)

        return suma_pesos

    def __iter__(self):
        return self._vertices.__iter__()


########################################################################
#                        GRAFO DIRIGIDO
########################################################################


class GrafoDirigido(GrafoBase):

    """Modela un grafo dirigido.

    Extiende de su clase base:
        * borrar_vertice
        * agregar_arista
        * borrar_arista

    """

    def borrar_vertice(self, v):
        """Borra del grafo el vértice pasado por parámetro.

        :param v: Vértice que se quiere borrar.
        :return None:
        :raises ValueError si el vértice no pertenece al grafo.

        """

        GrafoBase.borrar_vertice(self, v)

        for vertice in self._vertices:
            if self.es_adyacente(v, vertice):
                self._vertices[vertice].pop(v)

        self._vertices.pop(v)
        self._cantidad_vertices -= 1

    def agregar_arista(self, v, w, peso=1):
        """Agrega al grafo la arista pasada por parámetro, con el peso
        pasado por parámetro. Si la arista ya pertence al grafo,
        sustituye el valor del peso.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :param peso: Peso asociado a la arista.
        :return: None.
        :raises: ValueError si alguno de los vértices no pertenece al
        grafo.

        """

        GrafoBase.agregar_arista(self, v, w, peso)

        self._vertices[v][w] = peso

    def borrar_arista(self, v, w):
        """Borra del grafo la arista pasada por parámetro.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :return: None
        :raises:
            ValueError si alguno de los vértices no pertence al grafo.
            ValueError si la arista no pertenece al grafo.

        """

        GrafoBase.borrar_arista(self, v, w)

        self._vertices[v].pop(w)

    def obtener_peso_total(self):
        """Devuelve la suma del peso de todas las aristas del grafo. """

        return GrafoBase.obtener_peso_total(self)


########################################################################
#                       GRAFO NO DIRIGIDO
########################################################################

class GrafoNoDrigido(GrafoBase):

    """Modela un grafo no dirigido.

    Extiende de su clase base:
        * borrar_vertice
        * agregar_arista
        * borrar_arista

    """

    def borrar_vertice(self, v):
        """Borra del grafo el vértice pasado por parámetro.

        :param v: Vértice que se quiere borrar.
        :return None:
        :raises ValueError si el vértice no pertenece al grafo.

        """

        GrafoBase.borrar_vertice(self, v)

        adyacentes = self.ver_adyacentes(v)
        for w in adyacentes:
            self._vertices[w].pop(v)

        self._vertices.pop(v)
        self._cantidad_vertices -= 1
        return True

    def agregar_arista(self, v, w, peso=1):
        """Agrega al grafo la arista pasada por parámetro, con el peso
        pasado por parámetro. Si la arista ya pertence al grafo,
        sustituye el valor del peso.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :param peso: Peso asociado a la arista.
        :return: None.
        :raises: ValueError si alguno de los vértices no pertenece al
        grafo.

        """

        GrafoBase.agregar_arista(self, v, w, peso)

        self._vertices[v][w] = peso
        self._vertices[w][v] = peso

    def borrar_arista(self, v, w):
        """Borra del grafo la arista pasada por parámetro.

        :param v: Vértice origen de la arista.
        :param w: Vértice destino de la arista.
        :return: None
        :raises:
            ValueError si alguno de los vértices no pertence al grafo.
            ValueError si la arista no pertenece al grafo.

        """

        GrafoBase.borrar_arista(self, v, w)

        self._vertices[v].pop(w)
        self._vertices[w].pop(v)

    def obtener_peso_total(self):
        """Devuelve la suma del peso de todas las aristas del grafo. """

        peso_total = GrafoBase.obtener_peso_total(self) / 2
        if peso_total.is_integer():
            return int(peso_total)
        return peso_total


########################################################################
#                      FUNCIONES AUXILIARES
########################################################################

def calcular_costo_total(grafo, recorrido):
    """Calcula el costo total del recorrido pasado por parámetro. El
    recorrido es un recorrido válido del grafo.

    :param grafo: Grafo al que pertence el recorrido.
    :param recorrido: Recorrido cuyo costo total se quiere calcular.
    :return: Devuelve el costo total del recorrido.

    """

    costo = 0
    for i in range(0, len(recorrido) - 1):
        v = recorrido[i]
        w = recorrido[i + 1]
        peso = grafo.obtener_peso_arista(v, w)
        costo += peso
    return costo



