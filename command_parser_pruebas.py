import unittest
from command_parser import CommandParser


class TestCommandParser(unittest.TestCase):

    def test_ir(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        command_parser.parse("ir Moscu, Saransk")
        self.assertEqual(command_parser.nombre_comando(), "ir")
        self.assertEqual(command_parser.parametros(), ("Moscu", "Saransk"))

        # act / assert
        command_parser.parse("ir Moscu, Nizhni Novgorod")
        self.assertEqual(command_parser.nombre_comando(), "ir")
        self.assertEqual(command_parser.parametros(), ("Moscu", "Nizhni Novgorod"))

        # act / assert
        command_parser.parse("ir Nizhni Novgorod, Moscu")
        self.assertEqual(command_parser.nombre_comando(), "ir")
        self.assertEqual(command_parser.parametros(), ("Nizhni Novgorod", "Moscu"))

    def test_viaje_optimo(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        command_parser.parse("viaje optimo, Saransk")
        self.assertEqual(command_parser.nombre_comando(), "viaje optimo")
        self.assertEqual(command_parser.parametros(), "Saransk")

        # act / assert
        command_parser.parse("viaje optimo, Nizhni Novgorod")
        self.assertEqual(command_parser.nombre_comando(), "viaje optimo")
        self.assertEqual(command_parser.parametros(), "Nizhni Novgorod")

    def test_viaje_aproximado(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        command_parser.parse("viaje aproximado, Saransk")
        self.assertEqual(command_parser.nombre_comando(), "viaje aproximado")
        self.assertEqual(command_parser.parametros(), "Saransk")

        # act / assert
        command_parser.parse("viaje aproximado, Nizhni Novgorod")
        self.assertEqual(command_parser.nombre_comando(), "viaje aproximado")
        self.assertEqual(command_parser.parametros(), "Nizhni Novgorod")

    def test_itinerario(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        command_parser.parse("itinerario recomendaciones.csv")
        self.assertEqual(command_parser.nombre_comando(), "itinerario")
        self.assertEqual(command_parser.parametros(), "recomendaciones.csv")

    def test_reducir_caminos(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        command_parser.parse("reducir_caminos destino.csv")
        self.assertEqual(command_parser.nombre_comando(), "reducir_caminos")
        self.assertEqual(command_parser.parametros(), "destino.csv")

    def test_comando_invalido(self):

        # arrange
        command_parser = CommandParser()

        # act / assert
        with self.assertRaises(ValueError):
            command_parser.parse("llegar Moscu, Saransk")


if __name__ == "__main__":
    unittest.main()
