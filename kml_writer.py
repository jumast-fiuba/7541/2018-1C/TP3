

class KmlWriter:
    """Clase utilitaria para escribir un archivo kml.

    Permite:
        * agregar un punto.
        * agrega una ruta.

    """

    def __init__(self, archivo):
        self.nombre = archivo

    def agregar_punto(self, nombre_punto, coordenadas):
        """Agrega un punto en el mapa"""

        latitud, longitud = coordenadas
        with open(self.nombre, 'a') as mapa:
            mapa.write('\t\t<Placemark>\n')
            mapa.write('\t\t\t<name>{}</name>\n'.format(nombre_punto))
            mapa.write('\t\t\t<Point>\n')
            mapa.write('\t\t\t\t<coordinates>{}, {}</coordinates>\n'.format(longitud, latitud))
            mapa.write('\t\t\t</Point>\n')
            mapa.write('\t\t</Placemark>\n')
        return

    def agregar_ruta(self, coordenadas1, coordenadas2):
        """Agrega una ruta entre dos coordenadas recibidas por parámetro"""

        latitud1, longitud1 = coordenadas1
        latitud2, longitud2 = coordenadas2

        with open(self.nombre, 'a') as mapa:
            mapa.write('\t\t<Placemark>\n')
            mapa.write('\t\t\t<LineString>\n')
            mapa.write(
                '\t\t\t\t<coordinates>{}, {} {}, {}</coordinates>\n'.format(longitud1, latitud1, longitud2, latitud2))
            mapa.write('\t\t\t</LineString>\n')
            mapa.write('\t\t</Placemark>\n')
        return

    def iniciar(self):
        """Crea un archivo XML"""

        with open(self.nombre, 'w') as mapa:
            mapa.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            mapa.write('<kml xmlns="http://earth.google.com/kml/2.1">\n')
            mapa.write('\t<Document>\n')
            mapa.write('\t\t<name>{}</name>\n'.format(self.nombre))
        return

    def terminar(self):
        """Escribe las últimas líneas en el archivo XML"""

        with open(self.nombre, 'a') as mapa:
            mapa = open(self.nombre, 'a')
            mapa.write('\t</Document>\n')
            mapa.write('</kml>')
        return
