import unittest
from grafo import *


########################################################################
#                       GRAFO NO DIRIGIDO
########################################################################

# noinspection PyPep8Naming
class TestGrafoNoDirigido(unittest.TestCase):

    """Pruebas para GrafoNoDirigido"""

    def test_crear_grafo(self):

        # act
        grafo = GrafoNoDrigido()

        # assert
        self.assertEqual(grafo.cantidad_vertices(), 0)
        self.assertEqual(grafo.obtener_vertices(), set())
        self.assertFalse(grafo.vertice_en_grafo("A"))
        self.assertFalse(grafo.vertice_en_grafo("B"))

        with self.assertRaises(ValueError):
            grafo.borrar_vertice("A")

        with self.assertRaises(ValueError):
            grafo.es_adyacente("B", "A")

        with self.assertRaises(ValueError):
            grafo.obtener_info_vertice("A")

        with self.assertRaises(ValueError):
            grafo.borrar_arista("A", "B")

        with self.assertRaises(ValueError):
            grafo.borrar_aristas()

        with self.assertRaises(ValueError):
            grafo.obtener_peso_arista("A", "B")

        with self.assertRaises(ValueError):
            grafo.ver_adyacentes("A")

        with self.assertRaises(ValueError):
            grafo.vertice_aleatorio()

    def test_agregar_vertice(self):

        # arrange
        grafo = GrafoNoDrigido()

        # act
        grafo.agregar_vertice("A")

        # assert
        self.assertEqual(grafo.cantidad_vertices(), 1)
        self.assertTrue(grafo.vertice_en_grafo("A"))
        self.assertEqual(grafo.ver_adyacentes("A"), set())
        self.assertEqual(grafo.obtener_info_vertice("A"), None)
        self.assertEqual(grafo.obtener_vertices(), {"A"})
        self.assertEqual(grafo.vertice_aleatorio(), "A")

        with self.assertRaises(ValueError):
            grafo.es_adyacente("B", "A")

        with self.assertRaises(ValueError):
            grafo.borrar_arista("A", "B")

        with self.assertRaises(ValueError):
            grafo.obtener_peso_arista("A", "B")

    def test_agregar_vertices(self):

        # arange
        grafo = GrafoNoDrigido()

        # act: agregar cuatro vertices distintos
        grafo.agregar_vertice('A')
        grafo.agregar_vertice('B')
        grafo.agregar_vertice('C', "infoVerticeC")
        grafo.agregar_vertice('D', "infoVerticeD")

        # assert
        self.assertTrue(grafo.vertice_en_grafo("A"))
        self.assertTrue(grafo.vertice_en_grafo("B"))
        self.assertTrue(grafo.vertice_en_grafo("C"))
        self.assertTrue(grafo.vertice_en_grafo("D"))

        self.assertTrue(grafo.cantidad_vertices(), 4)

        self.assertFalse(grafo.vertice_en_grafo("E"))

        with self.assertRaises(ValueError):
            # agregar vertice que ya esta en el grafo
            grafo.agregar_vertice('C')

    def test_obtener_vertices(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice('A')
        grafo.agregar_vertice('B')
        grafo.agregar_vertice('C')
        grafo.agregar_vertice('D')

        # act
        vertices = grafo.obtener_vertices()

        # assert
        self.assertEqual(vertices, {"A", "B", "C", "D"})
        self.assertNotEqual(vertices, {"A", "B", "C", "D", "otro"})

    def test_vertice_aleatorio(self):

        # arange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C", "infoVerticeC")
        grafo.agregar_vertice("D", "infoVerticeD")

        # act:
        vertice_aleatorio = grafo.vertice_aleatorio()

        # assert
        self.assertTrue(vertice_aleatorio in ["A", "B", "C", "D"])
        self.assertFalse(vertice_aleatorio in ["A1", "B1", "C1", "D1"])

    def test_borrar_vertices(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        # act
        grafo.borrar_vertice("C")

        # assert
        self.assertFalse(grafo.vertice_en_grafo("C"))
        self.assertTrue(grafo.cantidad_vertices(), 3)
        self.assertEqual(grafo.obtener_vertices(), {"A", "B", "D"})

        with self.assertRaises(ValueError):
            # borrar vertice que no está
            grafo.borrar_vertice("C")

    def test_ver_adyacentes(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")
        grafo.agregar_arista("A", "B", 4)
        grafo.agregar_arista("A", "C", 3)
        grafo.agregar_arista("B", "C", 6)
        grafo.agregar_arista("B", "A", 6)
        grafo.agregar_arista("C", "D", 1)

        # act
        adyacentes_A = grafo.ver_adyacentes("A")
        adyacentes_B = grafo.ver_adyacentes("B")
        adyacentes_C = grafo.ver_adyacentes("C")
        adyacentes_D = grafo.ver_adyacentes("D")

        # assert
        self.assertEqual(adyacentes_A, {"B", "C"})
        self.assertEqual(adyacentes_B, {"A", "C"})
        self.assertEqual(adyacentes_C, {"A", "B", "D"})
        self.assertEqual(adyacentes_D, {"C"})

    def test_ver_adyacentes_2(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")
        grafo.agregar_vertice("E")

        grafo.agregar_arista("A", "B", 4)
        grafo.agregar_arista("A", "C", 3)
        grafo.agregar_arista("B", "C", 6)
        grafo.agregar_arista("B", "A", 6)
        grafo.agregar_arista("C", "D", 1)
        grafo.agregar_arista("C", "E", 1)
        grafo.agregar_arista("B", "D", 1)
        grafo.agregar_arista("B", "E", 1)
        grafo.agregar_arista("D", "E", 1)
        grafo.borrar_vertice("D")

        # act
        adyacentes_A = grafo.ver_adyacentes("A")
        adyacentes_B = grafo.ver_adyacentes("B")
        adyacentes_C = grafo.ver_adyacentes("C")
        adyacentes_E = grafo.ver_adyacentes("E")

        # assert
        self.assertEqual(adyacentes_A, {"B", "C"})
        self.assertEqual(adyacentes_B, {"A", "C", "E"})
        self.assertEqual(adyacentes_C, {"A", "B", "E"})
        self.assertEqual(adyacentes_E, {"B", "C"})

        with self.assertRaises(ValueError):
            grafo.ver_adyacentes("D")

    def test_agregar_aristas(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        # act
        grafo.agregar_arista("A", "B", 4)
        grafo.agregar_arista("B", "C", 6)
        grafo.agregar_arista("C", "A", 1)

        # assert
        with self.assertRaises(ValueError):
            grafo.agregar_arista("A", "verticeInexistente", 4)

        with self.assertRaises(ValueError):
            grafo.agregar_arista("verticeInexistente", "A", 4)

    def test_obtener_peso_arista(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        PESO_AB_ESPERADO = 4
        PESO_BC_ESPERADO = 6
        PESO_CD_ESPERADO = 3
        grafo.agregar_arista("A", "B", PESO_AB_ESPERADO)
        grafo.agregar_arista("B", "C", PESO_BC_ESPERADO)
        grafo.agregar_arista("C", "D", 45)
        grafo.agregar_arista("C", "D", PESO_CD_ESPERADO)

        # act
        peso_AB_obtenido = grafo.obtener_peso_arista("A", "B")
        peso_BC_obtenido = grafo.obtener_peso_arista("B", "C")
        peso_CD_obtenido = grafo.obtener_peso_arista("C", "D")

        peso_BA_obtenido = grafo.obtener_peso_arista("B", "A")
        peso_CB_obtenido = grafo.obtener_peso_arista("C", "B")
        peso_DC_obtenido = grafo.obtener_peso_arista("D", "C")

        # assert
        self.assertEqual(peso_AB_obtenido, PESO_AB_ESPERADO)
        self.assertEqual(peso_BC_obtenido, PESO_BC_ESPERADO)
        self.assertEqual(peso_CD_obtenido, PESO_CD_ESPERADO)

        self.assertEqual(peso_BA_obtenido, PESO_AB_ESPERADO)
        self.assertEqual(peso_CB_obtenido, PESO_BC_ESPERADO)
        self.assertEqual(peso_DC_obtenido, PESO_CD_ESPERADO)

        with self.assertRaises(ValueError):
            grafo.obtener_peso_arista("A", "verticeInexistente")

    def test_es_adyacente(self):

        # arrange
        grafo = GrafoNoDrigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        grafo.agregar_arista("A", "B", 2)
        grafo.agregar_arista("B", "C", 0)
        grafo.agregar_arista("C", "D", 1)

        # act
        A_adyacente_a_B = grafo.es_adyacente("A", "B")
        B_adyacente_a_A = grafo.es_adyacente("B", "A")
        B_adyacente_a_C = grafo.es_adyacente("B", "C")
        C_adyacente_a_B = grafo.es_adyacente("C", "B")

        A_adyacente_a_C = grafo.es_adyacente("A", "C")
        C_adyacente_a_A = grafo.es_adyacente("C", "B")

        # assert
        self.assertTrue(A_adyacente_a_B)
        self.assertTrue(B_adyacente_a_A)
        self.assertTrue(B_adyacente_a_C)
        self.assertTrue(C_adyacente_a_B)

        self.assertFalse(A_adyacente_a_C)
        self.assertTrue(C_adyacente_a_A)


########################################################################
#                        GRAFO DIRIGIDO
########################################################################

# noinspection PyPep8Naming
class TestGrafoDirigido(unittest.TestCase):
    """Pruebas complementarias para GrafoDirigido. """

    def test_obtener_peso_arista(self):

        # arrange
        grafo = GrafoDirigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        PESO_AB_ESPERADO = 4
        PESO_BC_ESPERADO = 6
        PESO_CD_ESPERADO = 3
        grafo.agregar_arista("A", "B", PESO_AB_ESPERADO)
        grafo.agregar_arista("B", "C", PESO_BC_ESPERADO)
        grafo.agregar_arista("C", "D", 45)
        grafo.agregar_arista("C", "D", PESO_CD_ESPERADO)

        # act
        peso_AB_obtenido = grafo.obtener_peso_arista("A", "B")
        peso_BC_obtenido = grafo.obtener_peso_arista("B", "C")
        peso_CD_obtenido = grafo.obtener_peso_arista("C", "D")

        # assert
        self.assertEqual(peso_AB_obtenido, PESO_AB_ESPERADO)
        self.assertEqual(peso_BC_obtenido, PESO_BC_ESPERADO)
        self.assertEqual(peso_CD_obtenido, PESO_CD_ESPERADO)

        with self.assertRaises(ValueError):
            grafo.obtener_peso_arista("A", "verticeInexistente")

        with self.assertRaises(ValueError):
            grafo.obtener_peso_arista("verticeInexistente", "A")

        with self.assertRaises(ValueError):
            # arista inexistente
            grafo.obtener_peso_arista("B", "A")

    def test_es_adyacente(self):

        # arrange
        grafo = GrafoDirigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")

        grafo.agregar_arista("A", "B", 2)
        grafo.agregar_arista("B", "C", 0)
        grafo.agregar_arista("C", "D", 1)

        # act
        A_adyacente_a_B = grafo.es_adyacente("A", "B")
        B_adyacente_a_A = grafo.es_adyacente("B", "A")
        B_adyacente_a_C = grafo.es_adyacente("B", "C")
        C_adyacente_a_B = grafo.es_adyacente("C", "B")

        A_adyacente_a_C = grafo.es_adyacente("A", "C")
        C_adyacente_a_A = grafo.es_adyacente("C", "B")

        # assert
        self.assertFalse(A_adyacente_a_B)
        self.assertTrue(B_adyacente_a_A)
        self.assertFalse(B_adyacente_a_C)
        self.assertTrue(C_adyacente_a_B)

        self.assertFalse(A_adyacente_a_C)
        self.assertTrue(C_adyacente_a_A)

    def test_ver_adyacentes(self):

        # arrange
        grafo = GrafoDirigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")
        grafo.agregar_arista("A", "B", 4)
        grafo.agregar_arista("A", "C", 3)
        grafo.agregar_arista("B", "C", 6)
        grafo.agregar_arista("B", "A", 6)
        grafo.agregar_arista("C", "D", 1)
        grafo.agregar_arista("B", "D", 1)
        grafo.agregar_arista("D", "B", 1)

        # act
        adyacentes_A = grafo.ver_adyacentes("A")
        adyacentes_B = grafo.ver_adyacentes("B")
        adyacentes_C = grafo.ver_adyacentes("C")
        adyacentes_D = grafo.ver_adyacentes("D")

        # assert
        self.assertEqual(adyacentes_A, {"C", "B"})
        self.assertEqual(adyacentes_B, {"A", "C", "D"})
        self.assertEqual(adyacentes_C, {"D"})
        self.assertEqual(adyacentes_D, {"B"})

    def test_ver_adyacentes_2(self):

        # arrange
        grafo = GrafoDirigido()
        grafo.agregar_vertice("A")
        grafo.agregar_vertice("B")
        grafo.agregar_vertice("C")
        grafo.agregar_vertice("D")
        grafo.agregar_vertice("E")

        grafo.agregar_arista("A", "B", 4)
        grafo.agregar_arista("A", "C", 3)
        grafo.agregar_arista("B", "C", 6)
        grafo.agregar_arista("B", "A", 6)
        grafo.agregar_arista("C", "D", 1)
        grafo.agregar_arista("C", "E", 1)
        grafo.agregar_arista("B", "D", 1)
        grafo.agregar_arista("B", "E", 1)
        grafo.agregar_arista("D", "A", 1)
        grafo.agregar_arista("D", "E", 1)
        grafo.agregar_arista("E", "A", 1)
        grafo.agregar_arista("E", "D", 1)
        grafo.borrar_vertice("D")

        # act
        adyacentes_A = grafo.ver_adyacentes("A")
        adyacentes_B = grafo.ver_adyacentes("B")
        adyacentes_C = grafo.ver_adyacentes("C")
        adyacentes_E = grafo.ver_adyacentes("E")

        # assert
        self.assertEqual(adyacentes_A, {"B", "C"})
        self.assertEqual(adyacentes_B, {"A", "C", "E"})
        self.assertEqual(adyacentes_C, {"E"})
        self.assertEqual(adyacentes_E, {"A"})

        with self.assertRaises(ValueError):
            grafo.ver_adyacentes("D")


if __name__ == "__main__":
    unittest.main()
