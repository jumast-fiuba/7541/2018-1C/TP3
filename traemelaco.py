import argparse
from biblioteca import *
from utils import *
from command_parser import CommandParser


def main(nombre_archivo_ciudades, nombre_archivo_kml):
    """Programa principal. """

    try:
        grafo_ciudades = leer_archivo_ciudades(nombre_archivo_ciudades)
    except FileNotFoundError:
        msg = 'Error. No se pudo leer el archivo "{}". Por favor verifique que el archivo exista.'\
            .format(nombre_archivo_ciudades)
        print(msg)
        return

    command_parser = CommandParser()

    while True:

        try:
            linea = input()
        except EOFError:
            return

        if linea == "":
            return

        try:
            command_parser.parse(linea)
        except ValueError:
            print('Comando desconocido.')
            return

        nombre_comando = command_parser.nombre_comando()

        if nombre_comando == "ir":

            desde, hasta = command_parser.parametros()

            recorrido = camino_minimo(grafo_ciudades, desde, hasta)
            costo_total = calcular_costo_total(grafo_ciudades, recorrido)

            imprimir_recorrido(recorrido, costo_total)
            exportar_recorrido(recorrido, grafo_ciudades, nombre_archivo_kml)

        if nombre_comando == "viaje optimo":

            origen = command_parser.parametros()

            recorrido = viajante(grafo_ciudades, origen)
            costo_total = calcular_costo_total(grafo_ciudades, recorrido)

            imprimir_recorrido(recorrido, costo_total)
            exportar_recorrido(recorrido, grafo_ciudades, nombre_archivo_kml)

        if nombre_comando == "viaje aproximado":

            origen = command_parser.parametros()

            recorrido = viajante_aproximado(grafo_ciudades, origen)
            costo_total = calcular_costo_total(grafo_ciudades, recorrido)

            imprimir_recorrido(recorrido, costo_total)
            exportar_recorrido(recorrido, grafo_ciudades, nombre_archivo_kml)

        if nombre_comando == "itinerario":

            nombre_archivo_recomendaciones = command_parser.parametros()
            grafo_recomendaciones = leer_archivo_recomendaciones(nombre_archivo_recomendaciones, grafo_ciudades)

            recorrido = orden_topologico_bfs(grafo_recomendaciones)
            costo_total = calcular_costo_total(grafo_ciudades, recorrido)

            imprimir_recorrido(recorrido, costo_total)
            exportar_recorrido(recorrido, grafo_ciudades, nombre_archivo_kml)

        if nombre_comando == "reducir_caminos":

            archivo_destino = command_parser.parametros()

            arbol = arbol_tendido_minimo(grafo_ciudades)
            peso_total = arbol.obtener_peso_total()

            escribir_archivo_ciudades(archivo_destino, arbol)
            imprimir_peso_total(peso_total)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="traemelaco")
    parser.add_argument("nombre_archivo_ciudades", type=str, help="Formato: csv. Nombre del archivo con las ciudades.")
    parser.add_argument("nombre_archivo_mapa", type=str, help="Formato: kml. Nombre del archivo que se crea al exportar.")
    args = parser.parse_args()

    main(args.nombre_archivo_ciudades, args.nombre_archivo_mapa)
