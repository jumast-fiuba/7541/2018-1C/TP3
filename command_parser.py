

class CommandParser:
    """Clase para parsear los comandos ingresados por el el usuario """

    def __init__(self):

        self._comandos = {
            "ir": 2,
            "viaje optimo": 1,
            "viaje aproximado": 1,
            "itinerario": 1,
            "reducir_caminos": 1,
        }
        """Diccionario en el que cada clave es el nombre de un comando
        válido, y cada valor es la cantidad de parámetros requerida por
        el comando.
        
        """

        self._nombre_comando = ""
        self._parametros = None

    def parse(self, linea):
        """Interpreta la cadena pasada por parámetro para crear a partir
        de ella un comando válido. Si el comando es válido, asigna
        valores a self._nombre_comando y self._parametros.

        :param linea: Cadena de texto que se quiere parsear.
        :return: None
        :raises: Value error si la línea no representar un comando
        válido.

        """

        comma_splitted = linea.split(",")
        primero = comma_splitted[0]

        # viaje optimo / viaje aproximado
        if primero == "viaje optimo" or primero == "viaje aproximado":
            origen = comma_splitted[1].lstrip()

            self._nombre_comando = primero
            self._parametros = origen

            return

        # ir
        nombre_comando = primero.split()[0]
        if nombre_comando == "ir":

            parametros = linea[len(nombre_comando):].split(",")
            desde = parametros[0]
            hasta = parametros[1]
            self._nombre_comando = nombre_comando
            self._parametros = (desde.lstrip(), hasta.lstrip())

            return

        # itinerario / reducir_caminos
        splitted = linea.split()
        nombre_comando = splitted[0]
        if nombre_comando in ["itinerario", "reducir_caminos"]:

            self._nombre_comando = nombre_comando
            self._parametros = splitted[1]

            return

        raise ValueError()

    def nombre_comando(self):
        """Devuelve el nombre del comando. """

        return self._nombre_comando

    def parametros(self):
        """Devuelve los parámetros del comando. Si el comando tiene
        solamente un parámetro, se lo devuelve directamente, si tiene
        más de un parámetro, se devuelve una tupla"""

        return self._parametros
