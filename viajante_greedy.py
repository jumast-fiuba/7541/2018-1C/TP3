import math


def viajante_greedy(grafo, origen):
    """ Calcula el recorrido a hacer para resolver de manera aproximada
    el problema del viajante, utilizando una técnica greedy.
    Ver: https://en.wikipedia.org/wiki/Travelling_salesman_problem.

    :param grafo:
    :param origen: Vértice origen.
    :return (tuple): Devuelve una tupla. El primer elemento es una lista
    de todos los vértices que definen el recorrido, y el segundo
    elemento es el costo total del recorrido.

    Ejemplo:
        >> viajante_greedy(grafoSedesMundialRusia, Sochi)
        ([Sochi, Volgogrado, Kazan, Nizhni Novgorod, Rostov del Don,
        Samara, Moscu, Kaliningrado, San Petesburgo, Saransk,
        Ekaterinburgo, Sochi], 57)

    """

    visitados = set()
    visitados_cantidad = 0
    vertices_cantidad = grafo.cantidad_vertices()

    camino = []
    peso_total = 0

    vertice = origen
    while True:

        if vertice in visitados:
            continue

        camino.append(vertice)
        visitados.add(vertice)
        visitados_cantidad += 1

        if visitados_cantidad == vertices_cantidad:
            break

        vecino, peso = _vecino_mas_cercano(vertice, grafo, visitados)
        peso_total += peso
        vertice = vecino

    camino.append(origen)
    peso_total += grafo.obtener_peso_arista(vertice, origen)

    return camino, peso_total


def _vecino_mas_cercano(vertice, grafo, visitados):
    peso_minimo = math.inf
    adyacente_peso_minimo = None

    for adyacente in grafo.ver_adyacentes(vertice):

        if adyacente in visitados:
            continue

        peso_adyacente = grafo.obtener_peso_arista(vertice, adyacente)
        if peso_adyacente < peso_minimo:
            peso_minimo = peso_adyacente
            adyacente_peso_minimo = adyacente

    return adyacente_peso_minimo, peso_minimo
