import csv
from grafo import *
from kml_writer import KmlWriter


def leer_archivo_ciudades(archivo_ciudades):
    """Crea un grafo no dirigido con los vertices y aristas
    especificados en el archivo pasado por parámetro.

    :param archivo_ciudades: Nombre del archivo csv con las coordenadas
    de las ciudades y el tiempo requerido para trasladarse entre ellas.
    Debe tener el siguiente formato:

    #Cantidad de ciudades
    Ciudad, latitud, longitud
    ...
    #Cantidad de aristas
    Ciudad1, Ciudad2, tiempo
    ...

    Ejemplo:

    3
    Moscu,55.755833,37.617778
    San Petesburgo,59.950000,30.316667
    Kaliningrado,54.716667,20.500000
    55
    Moscu,San Petesburgo,4
    Moscu,Kaliningrado,3
    San Petesburgo,Kaliningrado,4

    :return: Grafo no dirigido con los vertices y aristas especificados
    en el archivo.
    """

    grafo_ciudades = GrafoNoDrigido()

    with open(archivo_ciudades) as ciudades:
        ciudades_csv = csv.reader(ciudades)

        # agregar ciudades
        cant_ciudades = int(next(ciudades_csv, None)[0])
        for x in range(0, cant_ciudades):
            ciudad, latitud, longitud = next(ciudades_csv, None)
            grafo_ciudades.agregar_vertice(ciudad, (latitud, longitud))

        # agregar_aristas
        cant_aristas = int(next(ciudades_csv, None)[0])
        for x in range(0, cant_aristas):
            ciudad1, ciudad2, tiempo = next(ciudades_csv, None)
            grafo_ciudades.agregar_arista(ciudad1, ciudad2, int(tiempo))

    return grafo_ciudades


def leer_archivo_recomendaciones(archivo_recomendaciones, grafo_ciudades):
    """Crea un grafo dirigido con los vertices del grafo_ciudades y las
    aristas especificadas en el archivo_recomendaciones.

    :param archivo_recomendaciones: Nombre del archivo de
    recomendaciones. Es un archivo csv con dos ciudades por línea, e
    indica que la primera debe visitarse antes que la segunda.
    :param grafo_ciudades:
    :return: Grafo dirigido con los vertices del grafo_ciudades y las
    aristas especificadas en el archivo_recomendaciones.

    """

    ciudades = grafo_ciudades.obtener_vertices()
    grafo_recomendaciones = GrafoDirigido()

    # agregar todos los vertices
    for ciudad in ciudades:
        grafo_recomendaciones.agregar_vertice(ciudad, grafo_ciudades.obtener_info_vertice(ciudad))

    # agregar recomendaciones
    with open(archivo_recomendaciones) as recom:
        recom_csv = csv.reader(recom)
        recomendacion = next(recom_csv, None)

        while recomendacion:
            ciudad_1 = recomendacion[0]
            ciudad_2 = recomendacion[1]
            peso = grafo_ciudades.obtener_peso_arista(ciudad_1, ciudad_2)

            grafo_recomendaciones.agregar_arista(ciudad_1, ciudad_2, peso)
            recomendacion = next(recom_csv, None)

    return grafo_recomendaciones


def escribir_archivo_ciudades(nombre_archivo, grafo_ciudades):
    """Crea un archivo csv con las coordenadas de las ciudades y el
    tiempo requerido para trasladarse entre ellas, de acuerdo a los
    vértices y aristas del grafo.

    :param nombre_archivo: Nombre del archivo csv.
    :param grafo_ciudades:

    El archivo csv tendrá el siguiente formato:

    #Cantidad de ciudades
    Ciudad, latitud, longitud
    ...
    #Cantidad de aristas
    Ciudad1, Ciudad2, tiempo
    ...

    Ejemplo:

    3
    Moscu,55.755833,37.617778
    San Petesburgo,59.950000,30.316667
    Kaliningrado,54.716667,20.500000
    55
    Moscu,San Petesburgo,4
    Moscu,Kaliningrado,3
    San Petesburgo,Kaliningrado,4

    :return: None
    """

    cantidad_ciudades = grafo_ciudades.cantidad_vertices()
    with open (nombre_archivo, 'w') as archivo_ciudades:
        archivo_ciudades.write('{}\n'.format(cantidad_ciudades))

        for ciudad in grafo_ciudades:
            latitud, longitud = grafo_ciudades.obtener_info_vertice(ciudad)
            archivo_ciudades.write('{},{},{}\n'.format(ciudad, latitud, longitud))
      
        archivo_ciudades.write('{}\n'.format(cantidad_ciudades-1))
        ciudades = []
        
        for ciudad in grafo_ciudades:
            vertices_adyacentes = grafo_ciudades.ver_adyacentes(ciudad)
            for adyacente in vertices_adyacentes:
                if adyacente not in ciudades:
                    archivo_ciudades.write('{},{},{}\n'.format(ciudad, adyacente, grafo_ciudades.obtener_peso_arista(ciudad, adyacente)))
            ciudades.append(ciudad)


def exportar_recorrido(recorrido, grafo_ciudades, nombre_archivo):
    """Exporta a un archivo kml el recorrido pasado por parámetro.

    :param recorrido: Lista con los vértices del grafo que definen el
    recorrido que se quiere exportar.
    :param grafo_ciudades: Grafo con las rutas entre las ciudades.
    :param nombre_archivo: Nombre que tendrá el archivo creado.
    :return: None.

    """
    kml_writer = KmlWriter(nombre_archivo)
    kml_writer.iniciar()

    for i in range(0, len(recorrido) - 1):
        kml_writer.agregar_punto(recorrido[i], grafo_ciudades.obtener_info_vertice(recorrido[i]))
        kml_writer.agregar_ruta(grafo_ciudades.obtener_info_vertice(recorrido[i]), grafo_ciudades.obtener_info_vertice(recorrido[i + 1]))

    kml_writer.agregar_punto(recorrido[-1], grafo_ciudades.obtener_info_vertice(recorrido[-1]))

    kml_writer.terminar()
    return


def imprimir_recorrido(recorrido, costo_total):
    """Imprime por pantala el recorrido pasado por parámetro y su
    costo total.

    :param recorrido: Recorrido que se quiere imprimir.
    :param costo_total: Costo total del recorrido.
    :return: None.

    Ejemplo:

    >> recorrido = ["Moscu", "Samara", "Saransk"]
    >> costo_total = 8
    >> imprimir_recorrido(recorrido, costo_total)
    >>  Moscu -> Samara -> Saransk
        Costo total: 8

    """

    print(" -> ".join(recorrido))
    print("Costo total: {}".format(costo_total))


def imprimir_peso_total(peso_total):
    """Imprime por pantala el peso total pasado por parámetro.

    :param peso_total: Peso del recorrido.
    :return: None.

    Ejemplo:

    >> imprimir_peso_total(8)
    >> Peso total: 8

    """
    print("Peso total: {}".format(peso_total))
