import queue


def orden_topologico_bfs(grafo):
    """Calcula un orden topológico del grafo pasado por parámetro.

    :param grafo: Grafo dirigido.
    :return (list): Devuelve una lista con el orden topológico si
    existe, y None si no existe.

    Ejemplo:
        >> orden_topologico(grafoSedesMundialRusiaConRecomendaciones)
        [Saransk, Ekaterinburgo, Moscu, Kaliningrado, San Petesburgo,
        Kazan, Nizhni Novgorod, Samara, Volgogrado, Sochi
        Rostov del Don]

    """

    grado_entrada = {}
    for vertice in grafo.obtener_vertices():
        grado_entrada[vertice] = 0

    for vertice in grafo.obtener_vertices():
        for w in grafo.ver_adyacentes(vertice):
            grado_entrada[w] += 1

    orden = []
    q = queue.Queue()

    for vertice in grafo.obtener_vertices():
        if grado_entrada[vertice] == 0:
            q.put(vertice)

    while not q.empty():
        vertice = q.get()
        orden.append(vertice)

        for w in grafo.ver_adyacentes(vertice):
            grado_entrada[w] -= 1

            if grado_entrada[w] == 0:
                q.put(w)

    if len(orden) != grafo.cantidad_vertices():
        return None

    return orden
